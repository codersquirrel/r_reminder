class RemoveDateAndTimeColumns < ActiveRecord::Migration[6.0]
  def change
    remove_column :reminders, :date, :date
    remove_column :reminders, :time, :time
  end
end
