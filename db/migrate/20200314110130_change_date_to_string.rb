class ChangeDateToString < ActiveRecord::Migration[6.0]
  def change
    change_column :reminders, :date, :string
  end
end
