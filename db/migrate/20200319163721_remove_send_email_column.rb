class RemoveSendEmailColumn < ActiveRecord::Migration[6.0]
  def change
    remove_column :reminders, :date, :string
  end
end
