class ChangeDateToDateObject < ActiveRecord::Migration[6.0]
  def change
    add_column :reminders, :send_date, :datetime
  end
end
