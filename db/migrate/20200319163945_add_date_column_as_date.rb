class AddDateColumnAsDate < ActiveRecord::Migration[6.0]
  def change
    add_column :reminders, :date, :date
  end
end
