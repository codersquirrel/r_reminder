# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  describe 'welcome' do
    let(:mail) { UserMailer.welcome(create(:user)) }

    it 'renders the headers' do
      expect(mail.subject).to eq('Welcome to my great Reminder App')
      expect(mail.to).to eq(['to@example.com'])
      expect(mail.from).to eq(['from@example.com'])
    end

    it 'renders the body' do
      expect(mail.body.encoded).to match('Welcome!')
    end
  end
end
