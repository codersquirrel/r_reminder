# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'the create reminder process', type: :feature do
  it 'leads to reminder form' do
    login_as(FactoryBot.create(:random_user))
    visit '/'
    expect(page).to have_content 'Home'

    click_on 'New Reminder'
  end

  it 'creates a new reminder' do
    login_as(FactoryBot.create(:random_user))

    visit 'reminders/new'
    expect(page).to have_content 'new reminder'
    expect(page).to have_content 'Title'
    fill_in 'reminder_title', with: 'newest title'
    fill_in 'reminder_description', with: 'New Description'
    fill_in 'reminder_send_date', with: '2020-03-19 11:00:00'

    click_on 'Create Reminder'
    expect(page).to have_content 'All my Reminders'
  end
end
