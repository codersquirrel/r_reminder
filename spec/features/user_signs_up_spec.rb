# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'the sign-up process', type: :feature do
  it 'signs up new user' do
    visit '/users/sign_up'

    fill_in 'Email', with: 'user@example.com'
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'

    click_button 'Sign Up'
    expect(page).to have_content 'Home'
  end
end
