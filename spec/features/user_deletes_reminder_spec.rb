# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'the delete reminder process', type: :feature do
  scenario 'success' do
    login_as(FactoryBot.create(:random_user))
    create(:reminder)
    visit '/'
    expect(page).to have_content('2020-03-19 11:00:00')
    click_on 'Delete'
    expect(page).not_to have_content '2020-03-19 11:00:00'
  end
end
