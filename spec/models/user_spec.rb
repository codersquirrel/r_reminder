# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:password) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:reminders) }
  end

  describe 'user count' do
    it 'has 0 users at the start' do
      expect(User.count).to eq(0)
    end

    it 'has 1 user after #create' do
      create(:random_user)
      expect(User.count).to eq(1)
    end

    it 'has 0 users when using transactions' do
      expect(User.count).to eq(0)
    end
  end

  it 'is valid with a unique email address' do
    expect(create(:random_user)).to validate_uniqueness_of(:email).case_insensitive
  end
end
