# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reminder, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:send_date) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'accept destroy' do
    it { accept_nested_attributes_for(:user).allow_destroy(true) }
  end
end
