# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { 'to@example.com' }
    password { 'MyPassword' }
  end

  factory :mailgun_user, class: User do
    email { 'codersquirrelblntesting@gmail.com' }
    password { 'MyPassword' }
  end

  factory :random_user, class: User do
    email { Faker::Internet.safe_email }
    password { 'MyPassword' }
  end
end
