# frozen_string_literal: true

FactoryBot.define do
  factory :reminder do
    title { Faker::Name.first_name }
    description { 'MyDescription' }
    send_date { '2020-03-19 11:00:00' }
    user
  end
end
