# README

### r_reminder
This web application will help you to never forget anything ever again. You can create an account and create reminders with a specific date and time, and the reminder email will be sent to you on a monthly basis.

##### Motivation
This coding challenge was an excellent opportunity to learn more about Date/Time objects, TDD (rspec) and cron jobs in conjunction with the ‘whenever’ gem and much more.

#### Tech framework used
*back-end:* Rails 6.0.2.1, ruby 2.6.5, gems: 'devise', 'whenever'

*for testing:* gems: 'capybara', 'shoulda-matchers', 'factory_bot_rails'

*front-end:* gem 'flatpickr'

### Getting started

#### How to install
Clone the repository in your terminal, and run

```zsh
$ git clone git@gitlab.com:codersquirrel/r_reminder.git
```

Wait patiently, then run

```zsh
$ bundle install
```

#### Database
run 
```zsh
$ rails db:create
$ rails db:migrate
```

#### `cron job` and `whenever` gem 
To enable the whenever gem to do its job and the cron job to work
<!--```zsh-->
<!--$ bundle exec wheneverize .-->
<!--```-->

When in development mode, run:
```zsh
$ whenever --update-crontab --set environment=development
```

After making changes to the `schedule.rb` file, always run
```zsh
$ whenever —update-crontab
```

To stop the cron jobs for any reason, run
```zsh
$ whenever -c and crontab -l
```

[More information](https://github.com/javan/whenever) about the `whenever` gem

##### Mailgun
In order for you to actually send emails, you can use [MailGun](https://www.mailgun.com/?utm_term=mailgun&utm_campaign=750089235&utm_content=&utm_source=google&utm_medium=cpc&hsa_grp=44926653532&hsa_cam=750089235&hsa_mt=e&hsa_net=adwords&hsa_ver=3&hsa_acc=2217295277&hsa_ad=389456816150&hsa_src=g&hsa_tgt=kwd-41599135362&hsa_kw=mailgun&gclid=EAIaIQobChMI0p3x3u6o6AIV24FaBR39ow0PEAAYASAAEgLt-vD_BwE). 
Here is a quick setup
* make an account
* go to the dashboard
![adfadsf](./app/assets/images/1mailgun_dashboard.png)
* click in *settings* and go to *API security*
![adfadsf](./app/assets/images/2mailgun_API.png)

* Go to *Sending* and then subcategory *Domain* to retrieve the information about the sandbox domain to try out your code
![adfadsf](./app/assets/images/3mailgun_sandbox.png)

* Make as .env file (please remember the dot at the beginning, so you do not share sensitive information on your GitLab account), and add
```ruby
API_MAILGUN=YOUR_API_KEY
MYDOMAIN=sandboxA_BUNCH_OF_NUMS_AND_LETTERS.mailgun.org
```

##### Sending emails - validate email addresses
The email recipients have to agree to get emails from MailGun:
Send email to the person, ask them to verify they want to receive emails from you
![adfadsf](./app/assets/images/4mailgun_authorize_email.png)
![adfadsf](./app/assets/images/5mailgun_email_unverified.png)
![adfadsf](./app/assets/images/6mailgun_email_verified.png)

##### Time Zone
TimeZone will always default to UTC (Coordinated Universal Time, also known as  Greenwich Mean Time (GMT))
I changed my time zone to Berlin, if your time zone is different, you can adjust it in these two files:
app/models/reminder.rb:18
```ruby
time_now = Time.current.in_time_zone("YOURTIMEZONE").strftime("%H:%M")
time_now = Time.current.in_time_zone("Mexico City").strftime("%H:%M")
```

And
config/application.rb:36
```ruby
config.time_zone = "YOURTIMEZONE"
```

#### Tests

To run all the tests, go to your console and run
```zsh
$ rake
```

To run a specific test, run spec spec/path/file.rb
ex.: run tests for the reminderModel, run 
```zsh
$ rspec spec/models/reminder_spec.rb 
```

To run a specific line in a specific test file, run 
spec/path/file.rb:line
Ex: line 7 in the test file for the reminderModel, run
```zsh
$ rspec spec/models/reminder_spec.rb:7 
```


#### Contribute
I am not done with this project, so if you feel motivated to contribute, feel free!


#### License
MIT


