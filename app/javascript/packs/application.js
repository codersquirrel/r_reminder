require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")

import 'popper.js';
import "bootstrap";
import flatpickr from 'flatpickr';

flatpickr(".datetime", {
  minDate: "today",
  enableTime: true,
});
