# frozen_string_literal: true

class RemindersController < ApplicationController
  def index
  end

  def new
    @reminder = Reminder.new
  end

  def create
    @reminder = Reminder.new(reminder_params)
    @reminder.user = current_user
    if @reminder.save
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    @reminder = Reminder.find(params[:id])
    @reminder.destroy
    redirect_to root_path
  end

  private

  def reminder_params
    params.require(:reminder).permit(:title, :description, :send_date)
  end
end
