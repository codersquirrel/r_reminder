# frozen_string_literal: true

class UserMailer < ApplicationMailer
  default from: 'from@example.com'

  def welcome(user)
    @user = user
    mail to: user.email, subject: 'Welcome to my great Reminder App'
  end

  def reminder_email(user, reminder)
    @user = user
    @reminder = reminder
    mail to: user.email, subject: 'Your Reminder'
  end
end
