# frozen_string_literal: true

class Reminder < ApplicationRecord
  belongs_to :user
  validates :title, presence: true
  validates :send_date, presence: true

  def self.send_email_reminder
    today_day = Time.current.day
    today_month_length = Time.current.end_of_month.day
    time_now = Time.current.in_time_zone('Berlin').strftime('%H:%M')

    Reminder.all.each do |remind|
      reminder_date = remind.send_date.day
      reminder_time = remind.send_date.strftime('%H:%M')

      if today_month_length < reminder_date
        reminder_date = today_month_length
      end

      if (reminder_date == today_day) && (reminder_time == time_now)
        UserMailer.reminder_email(remind.user, remind).deliver_now
      end
    end
  end
end
