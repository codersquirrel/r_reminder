class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # This line is commented out, because the email service MailGun provides a
  # sandbox domain which does not allow unauthorized email addresses to receive
  # emails from them. Hence it will make the rspec tests fail
  # after_create :send_welcome # uncomment line if you want to receive the email

  has_many :reminders, dependent: :destroy
  validates :email, presence: true, uniqueness: { case_sensitive: false }
  validates :password, presence: true

  private

  def send_welcome
    UserMailer.welcome(self).deliver_now
  end
end
