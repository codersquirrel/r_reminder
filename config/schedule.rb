# frozen_string_literal: true

env :PATH, ENV['PATH']
env :GEM_PATH, ENV['GEM_PATH']
set :job_template, "bash -c -u ':job'"

every 1.minute do
  runner 'Reminder.send_email_reminder', output: { error: 'error.log', standard: 'cron.log' }
end
