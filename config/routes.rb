# frozen_string_literal: true

Rails.application.routes.draw do

  root to: 'pages#home'
  devise_for :users, controllers: { registrations: 'users/registrations' }

  # resources :users, only: [:new, :create, :show]

  resources :reminders, only: [ :index, :new, :create, :show, :destroy ]
end
